package com.zongtui.fourinone.base.config.xml;

import com.zongtui.fourinone.file.FileAdapter;
import org.xml.sax.InputSource;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;

//import com.file.*;
//import com.base.*;
//import com.log.LogUtil;
//import javax.servlet.*;
//import javax.servlet.http.*;
//import com.lang.MulLangBean;

/**
 * ClassName: XmlUtil <br/>
 * Function: XML处理. <br/>
 * date: 2015-4-10 上午11:11:19 <br/>
 *
 * @author feng
 * @since JDK 1.7
 */
public class XmlUtil {
    /**
     * getXmlPropsByFile:得到文件属性. <br/>
     *
     * @param filePath 文件路径
     * @return 属性列表
     * @author feng
     * @since JDK 1.7
     */
    public ArrayList getXmlPropsByFile(String filePath) {
        return getXmlPropsByFile(filePath, null, null);
    }

    /**
     * 得到xml中某个属性的列表值. <br/>
     *
     * @param filePath     文件路径
     * @param propsRowDesc 属性名
     * @return 属性值列表
     */
    public ArrayList getXmlPropsByFile(String filePath, String propsRowDesc) {
        return getXmlPropsByFile(filePath, propsRowDesc, null);
    }

    /**
     * getXmlPropsByFile:得到属性文件. <br/>
     *
     * @param filePath     文件路径
     * @param propsRowDesc 属性名
     * @param keyDesc      属性描述.
     * @return 属性值列表
     * @author feng
     * @since JDK 1.7
     */
    public ArrayList getXmlPropsByFile(String filePath, String propsRowDesc, String keyDesc) {

        ArrayList list = new ArrayList();
        try {
            XmlCallback handler = new XmlCallback();
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser saxParser = factory.newSAXParser();
            InputSource src = new InputSource(new FileInputStream(filePath));

            if (propsRowDesc != null) {
                handler.setPropsRowDesc(propsRowDesc);
            }
            if (keyDesc != null) {
                handler.setKeyDesc(keyDesc);
            }

            saxParser.parse(src, handler);
            list = handler.getPropsList();
        } catch (Throwable t) {
            System.err.println("[XmlConfig][Error:get XmlProps From File]" + t);
        }
        return list;
    }


    /**
     * getXmlObjectByFile :得到属性对象.
     *
     * @param filePath 文件路径
     * @return 属性对象
     */
    public ArrayList getXmlObjectByFile(String filePath) {
        return getXmlObjectByFile(filePath, null, null);
    }

    /**
     * getXmlObjectByFile :得到属性对象.
     *
     * @param filePath     文件路径
     * @param propsRowDesc 属性名
     * @return 属性对象.
     */
    public ArrayList getXmlObjectByFile(String filePath, String propsRowDesc) {
        return getXmlObjectByFile(filePath, propsRowDesc, null);
    }

    /**
     * getXmlObjectByFile :得到属性对象.
     *
     * @param filePath     文件路径
     * @param propsRowDesc 属性名
     * @param keyDesc      属性描述.
     * @return 属性对象.
     */
    public ArrayList getXmlObjectByFile(String filePath, String propsRowDesc,
                                        String keyDesc) {
        ArrayList list = new ArrayList();
        try {
            XmlObjectCallback handler = new XmlObjectCallback();
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser saxParser = factory.newSAXParser();
            InputSource src = new InputSource(new FileInputStream(filePath));
            if (propsRowDesc != null)
                handler.setPropsRowDesc(propsRowDesc);
            if (keyDesc != null)
                handler.setKeyDesc(keyDesc);
            saxParser.parse(src, handler);
            list = handler.getObjList();
        } catch (FileNotFoundException fex) {
            System.err.println("[XmlConfig][Can't find config file:" + filePath + ",and create default config.xml in the path.]");
            FileAdapter fa = new FileAdapter(filePath);
            fa.getWriter().write(getDefaultConfig().getBytes());
            fa.close();
            return getXmlObjectByFile(filePath, propsRowDesc, keyDesc);
        } catch (Throwable t) {
            System.err.println("[XmlConfig][Error:get XmlObject From File]" + t);
        }
        return list;
    }

    /**
     * 获得默认配置.
     *
     * @return 默认配置
     */
    private String getDefaultConfig() {
        return "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" + "<propsTable desc=\"tableName\">\n" + "\t<propsRow desc=\"park\">\n" + "\t\t<service>ParkService</service>\n" + "\t\t<servers>localhost:1888,localhost:1889</servers>\n" + "\t\t<safeMemoryPer>0.95</safeMemoryPer>\n" + "\t\t<heartBeat>3000</heartBeat>\n" + "\t\t<maxDelay>30000</maxDelay>\n" + "\t\t<expiration>24</expiration>\n" + "\t\t<clearPeriod>0</clearPeriod>\n" + "\t\t<alwaysTryLeader>false</alwaysTryLeader>\n" + "\t\t<startWebApp>true</startWebApp>\n" + "\t</propsRow>\n" + "\t<propsRow DESC=\"coolHash\">\n" + "\t\t<dataRoot>data</dataRoot>\n" + "\t\t<keyLength desc=\"b\">256</keyLength>\n" + "\t\t<valueLength desc=\"m\">2</valueLength>\n" + "\t\t<regionLength desc=\"m\">2</regionLength>\n" + "\t\t<loadLength desc=\"m\">64</loadLength>\n" + "\t\t<hashCapacity>1000000</hashCapacity>\n" + "\t</propsRow>\n" + "\t<propsRow desc=\"cache\">\n" + "\t\t<service>CacheService</service>\n" + "\t\t<servers>localhost:2000,localhost:2001</servers>\n" + "\t</propsRow>\n" + "\t<propsRow desc=\"cacheGroup\">\n" + "\t\t<startTime>2010-01-01</startTime>\n" + "\t\t<group>localhost:2000,localhost:2001@2010-01-01;localhost:2002,localhost:2003@2010-05-01;localhost:2004,localhost:2005@2010-05-01</group>\n" + "\t</propsRow>\n" + "\t<propsRow desc=\"cacheGroup\">\n" + "\t\t<startTime>2018-05-01</startTime>\n" + "\t\t<group>localhost:2008,localhost:2009@2018-05-01;localhost:2010,localhost:2011@2018-05-01</group>\n" + "\t</propsRow>\n" + "\t<propsRow desc=\"cacheFacade\">\n" + "\t\t<service>CacheFacadeService</service>\n" + "\t\t<servers>localhost:1998</servers>\n" + "\t\t<tryKeysNum>100</tryKeysNum>\n" + "\t</propsRow>\n" + "\t<propsRow desc=\"worker\">\n" + "\t\t<timeout desc=\"false\">2</timeout>\n" + "\t\t<servers>localhost:2088</servers>\n" + "\t\t<service>false</service>\n" + "\t</propsRow>\n" + "\t<propsRow desc=\"ctor\">\n" + "\t\t<!-- <ctorServers>localhost:1988</ctorServers> -->\n" + "\t\t<initServices>20</initServices>\n" + "\t\t<maxServices>100</maxServices>\n" + "\t</propsRow>\n" + "\t<propsRow desc=\"computeMode\">\n" + "\t\t<mode desc=\"default\">0</mode>\n" + "\t\t<mode>1</mode>\n" + "\t</propsRow>\n" + "\t<propsRow desc=\"fttp\">\n" + "\t\t<servers>localhost:2121</servers>\n" + "\t</propsRow>\n" + "\t<propsRow desc=\"webApp\">\n" + "\t\t<servers>localhost:9080</servers>\n" + "\t\t<users>admin:admin,guest:123456,test:test</users>\n" + "\t</propsRow>\n" + "\t<propsRow desc=\"log\">\n" + "\t\t<levelName>all</levelName>\n" + "\t\t<levelName>severe</levelName>\n" + "\t\t<levelName>warning</levelName>\n" + "\t\t<levelName>info</levelName>\n" + "\t\t<levelName>config</levelName>\n" + "\t\t<levelName desc=\"logLevel\">FINE</levelName>\n" + "\t\t<levelName>FINER</levelName>\n" + "\t\t<levelName>FINERST</levelName>\n" + "\t\t<levelName>OFF</levelName>\n" + "\t\t<info>true</info>\n" + "\t\t<fine>false</fine>\n" + "\t</propsRow>\n" + "</propsTable>";
    }

    public static void main(String args[]) {
        XmlUtil xu = new XmlUtil();
        System.out.println(xu.getDefaultConfig());
    }
}