package com.zongtui.fourinone.park;

import com.zongtui.fourinone.base.BeanContext;
import com.zongtui.fourinone.file.FileResult;
import com.zongtui.fourinone.file.WareHouse;
import com.zongtui.fourinone.obj.ObjectBean;
import com.zongtui.fourinone.park.utils.AsyncExecutor;
import com.zongtui.fourinone.utils.log.LogUtil;

import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;

public class ParkPatternExector
{
	private static ParkLocal pl;
	private static LinkedBlockingQueue<ParkPatternBean> bq = new LinkedBlockingQueue<>();
	private static AsyncExecutor aeLastest=null;

	public static ParkLocal getParkLocal()
	{
		if(pl==null)
		{
			pl = BeanContext.getPark();
		}
		return pl;
	}

	static ParkLocal getParkLocal(String host, int port){
		return pl=pl==null?BeanContext.getPark(host, port):pl;
	}
	
	public static List<ObjectBean> getWorkerTypeList(String workerType)
	{
		return getParkLocal().get("_worker_"+workerType);
	}
	
	public static List<ObjectBean> getWorkerTypeList(String parkhost, int parkport, String workerType)
	{
		return getParkLocal(parkhost, parkport).get("_worker_"+workerType);
	}
	
	public static ObjectBean createWorkerTypeNode(String workerType, String nodevalue)
	{
		return getParkLocal().create("_worker_"+workerType, ParkGroup.getKeyId(), nodevalue, AuthPolicy.OP_ALL, true);
	}
	
	public static ObjectBean createWorkerTypeNode(String parkhost, int parkport, String workerType, String nodevalue)
	{
		return getParkLocal(parkhost, parkport).create("_worker_"+workerType, ParkGroup.getKeyId(), nodevalue, AuthPolicy.OP_ALL, true);
	}
	
	public static ObjectBean getLastestObjectBean(ObjectBean ob)
	{
		String[] keyarr = ParkObjValue.getDomainNode(ob.getName());
		while(true)
		{
			ObjectBean curob = getParkLocal().getLastest(keyarr[0], keyarr[1], ob);
			if(curob!=null)
				return curob;
		}
			
	}
	
	public static ObjectBean updateObjectBean(ObjectBean ob, WareHouse wh)
	{
		String[] keyarr = ParkObjValue.getDomainNode(ob.getName());
		return getParkLocal().update(keyarr[0], keyarr[1], wh);
	}
	
	public static void append(ParkPatternBean ppb)
	{
		try{
            ppb.thisversion = getParkLocal().update(ppb.domain, ppb.node, ppb.inhouse);
			bq.put(ppb);
			
			if(aeLastest==null){
				LogUtil.fine("", "", "AsyncExector aeLastest:");
				(aeLastest = new AsyncExecutor(){
					public void task(){
						try{
							while(true){
								ParkPatternBean curPpb = bq.take();
								//System.out.println("ParkPatternExector bq.size():"+bq.size());
								ObjectBean curversion = getParkLocal().getLastest(curPpb.domain, curPpb.node, curPpb.thisversion);
								if(curversion!=null)
								{
									curPpb.thisversion = curversion;
									curPpb.rx.setRecall(false);
									curPpb.outhouse.putAll((WareHouse)curversion.toObject());
									//curPpb.outhouse.setReady(true);
									curPpb.outhouse.setReady(FileResult.READY);
								}
								else
									bq.put(curPpb);
							}
						}catch(Exception e){
							LogUtil.info("AsyncExector", "append aeLastest", e);
							//e.printStackTrace();
						}
					}
				}).run();
			}
		}catch(Exception e){
			LogUtil.info("ParkPatternExector", "append", e);
		}
	}
}