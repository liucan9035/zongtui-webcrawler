<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>echarts demo管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
	</script>

</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/echartsdemo/echartsdemo/bar8">echarts demo列表</a></li>
	</ul>
	<form:form id="searchForm" modelAttribute="echartsdemo" action="${ctx}/echartsdemo/echartsdemo/bar8" method="post" class="breadcrumb form-search">
		<ul class="ul-form">
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	
	<div id="main" style="height:400px"></div>
	<script src="${ctxStatic}/echarts/echarts.js"></script>
<script type="text/javascript">
        // 路径配置
        require.config({
            paths: {
                echarts: '${ctxStatic}/echarts'
            }
        });
        
        // 使用
        require(
            [
                'echarts',
                'echarts/chart/line', // 使用线状图就加载line模块，按需加载
                'echarts/chart/bar' // 使用柱状图就加载bar模块，按需加载
            ],
            function (ec) {
                // 基于准备好的dom，初始化echarts图表
                var myChart = ec.init(document.getElementById('main')); 
                
                var option = {
                	    tooltip : {
                	        trigger: 'axis',
                	        axisPointer : {            // 坐标轴指示器，坐标轴触发有效
                	            type : 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
                	        }
                	    },
                	    legend: {
                	        data:['直接访问', '邮件营销','联盟广告','视频广告','搜索引擎']
                	    },
                	    toolbox: {
                	        show : true,
                	        feature : {
                	            mark : {show: true},
                	            dataView : {show: true, readOnly: false},
                	            magicType : {show: true, type: ['line', 'bar', 'stack', 'tiled']},
                	            restore : {show: true},
                	            saveAsImage : {show: true}
                	        }
                	    },
                	    calculable : true,
                	    xAxis : [
                	        {
                	            type : 'value'
                	        }
                	    ],
                	    yAxis : [
                	        {
                	            type : 'category',
                	            data : ['周一','周二','周三','周四','周五','周六','周日']
                	        }
                	    ],
                	    series : [
                	        {
                	            name:'直接访问',
                	            type:'bar',
                	            stack: '总量',
                	            itemStyle : { normal: {label : {show: true, position: 'insideRight'}}},
                	            data:[320, 302, 301, 334, 390, 330, 320]
                	        },
                	        {
                	            name:'邮件营销',
                	            type:'bar',
                	            stack: '总量',
                	            itemStyle : { normal: {label : {show: true, position: 'insideRight'}}},
                	            data:[120, 132, 101, 134, 90, 230, 210]
                	        },
                	        {
                	            name:'联盟广告',
                	            type:'bar',
                	            stack: '总量',
                	            itemStyle : { normal: {label : {show: true, position: 'insideRight'}}},
                	            data:[220, 182, 191, 234, 290, 330, 310]
                	        },
                	        {
                	            name:'视频广告',
                	            type:'bar',
                	            stack: '总量',
                	            itemStyle : { normal: {label : {show: true, position: 'insideRight'}}},
                	            data:[150, 212, 201, 154, 190, 330, 410]
                	        },
                	        {
                	            name:'搜索引擎',
                	            type:'bar',
                	            stack: '总量',
                	            itemStyle : { normal: {label : {show: true, position: 'insideRight'}}},
                	            data:[820, 832, 901, 934, 1290, 1330, 1320]
                	        }
                	    ]
                	};
        
                // 为echarts对象加载数据 
                myChart.setOption(option); 
            }
        );
    </script>
	
</body>
</html>